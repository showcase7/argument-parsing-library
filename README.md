# Argonaut
## Description
A simple argument parser for Rust.

The point of this project was to leverage the Rust compiler to point out errors in argument definitions during compile-time, instead of run-time, which was what the other argparsing libraries at the time did.

## Build instructions
Use the Rust build tool cargo, invoked with `cargo build`. 

## Usage
For examples of usage, see the 'examples' folder, or run the examples 'epub', 'main' and 'template_main' with `cargo run --example <example>`.

## License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
